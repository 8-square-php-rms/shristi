<?php
$url='http://localhost/shristi/';
$base_url='http://localhost/shristi/application/';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href= "<?php echo $url; ?>assets/css/style.css" rel ="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
<div class="main-container">
<header>
<div class="logo"><h5><img src="<?php echo $url; ?>image/logo.jpg" height="130px" width="130px">Pragati Prabhat Academy</h5></div>

</header>


<nav class="nav">
  <ul>
    <li>
      <a href="<?php echo $base_url ; ?>student/student.php">Student</a>
    </li>
    <li>
      <a href="<?php echo $base_url ; ?>class/class.php">Class</a>
    </li>
    <li>
      <a href="<?php echo $base_url ; ?>subject/subject.php">Subject</a>
    </li>
    <li>
      <a href="<?php echo $base_url ; ?>exam/examtype.php">Exam Type</a>
    </li>

    <li>
    <a href="<?php echo $base_url ; ?>marks/marks.php">Marks</a>
    </li>
    <li>
      <a href="<?php echo $base_url ; ?>result/find-result.php">Generate Report</a>
    </li>
  </ul>
</nav>

</div>


</body>
</html>